
const { successRes, errorRes } = require("../../data-control/dataResponses");

const redisWithPoints = [
    "5de3d570edfbe22da3731e66", "500",
    "5de3d570edfbe22da3731e67", "450",
    "5de3d570edfbe22da3731e68", "449",
    "5de3d570edfbe22da3731e69", "430",
    "5de3d570edfbe22da3731e5e", "411",
    "5de3d570edfbe22da3731e5f", "321",
    "5de3d570edfbe22da3731e60", "311",
    "5de3d570edfbe22da3731e6f", "256",
    "5de3d570edfbe22da3731e62", "246",
    "5de3d570edfbe22da3731e63", "222",
    "5de3d570edfbe22da3731e64", "190",
    "5de3d570edfbe22da3731e65", "187",
    "5de3d570edfbe22da3731e73", "176",
    "5de3d570edfbe22da3731e74", "156",
    "5de3d570edfbe22da3731e76", "109",
    "5de3d570edfbe22da3731e6a", "99",
    "5de3d570edfbe22da3731e6b", "92",
    "5de3d570edfbe22da3731e6c", "88",
    "5de3d570edfbe22da3731e6d", "81",
    "5de3d570edfbe22da3731e6e", "80",
    "5de3d570edfbe22da3731e75", "56",
    "5de3d570edfbe22da3731e70", "45",
    "5de3d570edfbe22da3731e71", "44",
    "5de3d570edfbe22da3731e72", "44",
    "5de3d570edfbe22da3731e61", "11",
]
const redisWithoutPoints = [
    "5de3d570edfbe22da3731e66",
    "5de3d570edfbe22da3731e67",
    "5de3d570edfbe22da3731e68",
    "5de3d570edfbe22da3731e69",
    "5de3d570edfbe22da3731e5e",
    "5de3d570edfbe22da3731e5f",
    "5de3d570edfbe22da3731e60",
    "5de3d570edfbe22da3731e6f",
    "5de3d570edfbe22da3731e62",
    "5de3d570edfbe22da3731e63",
    "5de3d570edfbe22da3731e64",
    "5de3d570edfbe22da3731e65",
    "5de3d570edfbe22da3731e73",
    "5de3d570edfbe22da3731e74",
    "5de3d570edfbe22da3731e76",
    "5de3d570edfbe22da3731e6a",
    "5de3d570edfbe22da3731e6b",
    "5de3d570edfbe22da3731e6c",
    "5de3d570edfbe22da3731e6d",
    "5de3d570edfbe22da3731e6e",
    "5de3d570edfbe22da3731e75",
    "5de3d570edfbe22da3731e70",
    "5de3d570edfbe22da3731e71",
    "5de3d570edfbe22da3731e72",
    "5de3d570edfbe22da3731e61",
]

module.exports = (redis) => ({
    getTotalUsers: (key) => {
        try{ 
            return successRes({ totalUsers: Math.floor(1000 + Math.random() * 9000) });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    },

    retrieveRevPointsList: (key, { start, stop }) => {
        try {
            return successRes({ redisData: redisWithPoints });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    },

    retrieveRevList: (key, { start, stop }) => {
        try {
            return successRes({ redisData: redisWithoutPoints });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    }


})