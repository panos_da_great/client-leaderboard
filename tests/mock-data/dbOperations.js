const { ObjectId }      = require('mongodb');
const { successRes, errorRes } = require("../../data-control/dataResponses");

const users = [
    {"_id":ObjectId("5de3d570edfbe22da3731e5e"),"__v":0,"country":"Sierra Leone","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/giuliusa/128.jpg","age":99,"name":"Odie Johnston"},
    {"_id":ObjectId("5de3d570edfbe22da3731e5f"),"__v":0,"country":"Central African Republic","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/samihah/128.jpg","age":95,"name":"Bernadine Ritchie"},
    {"_id":ObjectId("5de3d570edfbe22da3731e60"),"__v":0,"country":"Wallis and Futuna","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/iqbalperkasa/128.jpg","age":96,"name":"Brenna O'Connell"},
    {"_id":ObjectId("5de3d570edfbe22da3731e61"),"__v":0,"country":"Georgia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/cggaurav/128.jpg","age":18,"name":"Kristopher Gaylord"},
    {"_id":ObjectId("5de3d570edfbe22da3731e62"),"__v":0,"country":"Estonia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/maiklam/128.jpg","age":68,"name":"Boris Swift"},
    {"_id":ObjectId("5de3d570edfbe22da3731e63"),"__v":0,"country":"Mauritius","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/ssbb_me/128.jpg","age":86,"name":"Stacy Marks"},
    {"_id":ObjectId("5de3d570edfbe22da3731e64"),"__v":0,"country":"Australia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/byryan/128.jpg","age":55,"name":"Bartholome Larkin"},
    {"_id":ObjectId("5de3d570edfbe22da3731e65"),"__v":0,"country":"Montserrat","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/michaelcolenso/128.jpg","age":96,"name":"Xander Huel"},
    {"_id":ObjectId("5de3d570edfbe22da3731e66"),"__v":0,"country":"Mongolia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/krasnoukhov/128.jpg","age":92,"name":"George Jenkins"},
    {"_id":ObjectId("5de3d570edfbe22da3731e67"),"__v":0,"country":"Zambia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/daniloc/128.jpg","age":77,"name":"Bessie Kuhlman"},
    {"_id":ObjectId("5de3d570edfbe22da3731e68"),"__v":0,"country":"American Samoa","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/panghal0/128.jpg","age":93,"name":"Elena Weimann"},
    {"_id":ObjectId("5de3d570edfbe22da3731e69"),"__v":0,"country":"New Caledonia","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/hoangloi/128.jpg","age":64,"name":"Jennyfer Hayes"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6a"),"__v":0,"country":"Sweden","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/oscarowusu/128.jpg","age":67,"name":"Emiliano Beatty"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6b"),"__v":0,"country":"American Samoa","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/ryankirkman/128.jpg","age":56,"name":"Daryl Bosco"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6c"),"__v":0,"country":"Belize","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/jasonmarkjones/128.jpg","age":90,"name":"Chyna Bayer"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6d"),"__v":0,"country":"Northern Mariana Islands","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/alxndrustinov/128.jpg","age":20,"name":"Jennie Boehm"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6e"),"__v":0,"country":"Democratic People's Republic of Korea","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/brandonburke/128.jpg","age":95,"name":"Hobart Hilll"},
    {"_id":ObjectId("5de3d570edfbe22da3731e6f"),"__v":0,"country":"Belize","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/the_winslet/128.jpg","age":43,"name":"Shyann Ferry"},
    {"_id":ObjectId("5de3d570edfbe22da3731e70"),"__v":0,"country":"Denmark","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/marciotoledo/128.jpg","age":29,"name":"Johnpaul Becker"},
    {"_id":ObjectId("5de3d570edfbe22da3731e71"),"__v":0,"country":"Switzerland","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/pierrestoffe/128.jpg","age":22,"name":"Elroy Mitchell"},
    {"_id":ObjectId("5de3d570edfbe22da3731e72"),"__v":0,"country":"Zimbabwe","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/raphaelnikson/128.jpg","age":66,"name":"Jennifer Gusikowski"},
    {"_id":ObjectId("5de3d570edfbe22da3731e73"),"__v":0,"country":"Saint Barthelemy","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/areus/128.jpg","age":77,"name":"Abel Runolfsdottir"},
    {"_id":ObjectId("5de3d570edfbe22da3731e74"),"__v":0,"country":"Israel","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/mrjamesnoble/128.jpg","age":98,"name":"Sonny Mertz"},
    {"_id":ObjectId("5de3d570edfbe22da3731e75"),"__v":0,"country":"Djibouti","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/lebronjennan/128.jpg","age":56,"name":"Name Bergnaum"},
    {"_id":ObjectId("5de3d570edfbe22da3731e76"),"__v":0,"country":"Yemen","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/oscarowusu/128.jpg","age":30,"name":"Wilford Koss"}
]

module.exports = {
    fetchUsers: (objectIdsArr) => {
        try {
            return successRes({ userData: users });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    }
} 