const moment            = require("moment");

const mongo         = require("../controllers/mongo");

const dbOperations      = require("./mock-data/dbOperations");
const redisOperations   = require("./mock-data/redis")(null);

const Leaderboard       = require("../LeaderBoard");

const pageScenarios = [
    { req: { params: { page: 1 } },     res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 1,   to: 25 } } }, 
    { req: { params: { page: 2 } },     res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 26,  to: 50 } } },
    { req: { params: { page: 3 } },     res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 51,  to: 75 } } },
    { req: { params: { page: 4 } },     res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 76,  to: 100 } } },
    { req: { params: { page: 10 } },    res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 226, to: 250 } } },
    { req: { params: { page: "2" } },   res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 26,  to: 50 } } },
    { req: { params: { page: "1e2" } }, res: { getHeader: () => 1000 }, expecting: { dataLength:  0, error: "PAGE_DOES_NOT_EXIST",   ranking: null } },
    { req: { params: { page: "1b2" } }, res: { getHeader: () => 1000 }, expecting: { dataLength:  0, error: "INVALID_PAGE",   ranking: null } },
    { req: { params: { page: "1" } },   res: { getHeader: () => 1000 }, expecting: { dataLength: 25, error: null, ranking: { from: 1,   to: 25 } } },
    { req: { params: { page: 100000 } },res: { getHeader: () => 1000 }, expecting: { dataLength:  0, error: "PAGE_DOES_NOT_EXIST",   ranking: null } }, 
];

beforeAll(async() => {
    await mongo.awaitConnection();
})

describe("TEST LEADERBOARD CLASS", ()=> {
    const  {userData } = dbOperations.fetchUsers();
    const scenario = pageScenarios[0];

    const lb = new Leaderboard(scenario.req, scenario.res);

    test("formatUsers", async() => {
        

        const mockRedisData = userData.map(u => { return u._id.toString()})
        var points = 1;

        for(var i = mockRedisData.length; i > 0; i--) {
            mockRedisData.splice(i, 0, points++);
        }

        lb.redisData = mockRedisData;

        lb.formatUsers(userData);
        const data = lb.getData();

        expect(data.length).toBe(25);
        

    })
    
})

describe("LEADERBORD LOGIC TEST", () => {

    describe.each(pageScenarios)('Scenario Cases',({req, res, expecting}) => {

        try {
            const lbLogic = new Leaderboard(req, res);
            const { redisData } = redisOperations.retrieveRevPointsList("key", lbLogic.getPaginationIndexes())
            lbLogic.setRedisData(redisData);
            const { userData } = dbOperations.fetchUsers(lbLogic.getObjectIds());
            lbLogic.formatUsers(userData);
    
            test(`Requesting page: ${req.params.page} TEST`, () => {
                let data = lbLogic.getData();

                const fromRank = data[0].Rank;
                const toRank   = data[data.length - 1].Rank;

                expect(data.length).toBe(expecting.dataLength);
                expect(expecting.error).toBe(null);
                expect(fromRank).toBe(expecting.ranking.from);
                expect(toRank).toBe(expecting.ranking.to);
            })

            test(`Pagination Indexes Requesting page: ${req.params.page} TEST`, () => {
                const { start, stop } =  lbLogic.getPaginationIndexes();
                expect(start).toBe(expecting.ranking.from - 1);
                expect(stop).toBe(expecting.ranking.to - 1);
            })

        } catch(e) {
            test(`Failed Scenarios on Requesting page: ${req.params.page} TEST`, () => {
                expect(expecting.error).not.toBe(null);
                expect(e.message).toBe(expecting.error);
                expect(expecting.dataLength).toBe(0);
            })
        }

    })   
})