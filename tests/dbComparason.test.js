
const moment            = require("moment");
const { ObjectId }      = require('mongodb');

const mongo         = require("../controllers/mongo");
const RedisClient   = require("../controllers/redis");
const configuration = require('../config/configuration.json');

const redis         = new RedisClient(configuration.redis);

const dbOperations      = require("../data-control/dbOperations");
const redisOperations   = require("../data-control/redis")(redis);

const getRedisSetName = () => `lb_${moment.utc().format("YYYYMMDD-HH")}`;

beforeAll(async() => {
    await mongo.awaitConnection();
})

describe("Compare mongo ids in redis with ones in mongo", ()=> {
    const key = getRedisSetName();

    test("FETCH FROM MONGO 0 REDIS IDS", async() => {
        const { totalUsers } = await redisOperations.getTotalUsers(key);
        const { redisData } = await redisOperations.retrieveRevList(key, { start: 0, stop: totalUsers });

        const fetchIdsArr = redisData.map(_id => ObjectId(_id));

        console.log(fetchIdsArr.length);

        const countUsers = await mongo.Users().find({ _id: {$in: fetchIdsArr } }).count();
        console.log(countUsers);

        expect(countUsers).toBe(0);
    } )
})