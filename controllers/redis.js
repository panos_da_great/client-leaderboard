const moment   = require('moment');
const redis    = require('redis');
const bluebird = require('bluebird');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

class Redis { 

    constructor(redisConfiguration) {

        this.db     = redisConfiguration.db;
        this.client = redis.createClient(redisConfiguration);
        this.client.on('connect', function () {
            console.log(`> ${ moment().toISOString() } > redisController > Connection Established @ ${ JSON.stringify(redisConfiguration) }`);
        });

    }

    /**
     * Retrieve a sorted set from higher to lower score including scores
     * @param {*} key   Sorted set key name
     * @param {*} start Starting index
     * @param {*} stop  Last Index
     */
    retrieveRevPointsList(key, start, stop){
        return new Promise((resolve, reject) => {
            this.client.ZREVRANGE(key, start, stop, "WITHSCORES", (err, res) => {
                if (err){
                    reject(err);
                } 
                if (!res) {
                    console.log(`Redis > retrieveRevPointsList > ${key}> error: ${err}`);
                }
                resolve(res);
            })
        })

    }

    retrieveRevList(key, start, stop){
        return new Promise((resolve, reject) => {
            this.client.ZREVRANGE(key, start, stop, (err, res) => {
                if (err){
                    reject(err);
                } 
                if (!res) {
                    console.log(`Redis > retrieveRevPointsList > ${key}> error: ${err}`);
                }
                resolve(res);
            })
        })

    }

    getListLength(key, min, max) {
        return new Promise((resolve, reject) => {
            this.client.ZLEXCOUNT(key, min, max, (err, num) => {
                if (err) {
                    console.log(`Redis > getListLength > ${key} > error: ${err}`);
                }
                resolve(num);
            })
        })
    }

    async retrieveSet(key){
        return new Promise((resolve, reject) => {
            this.client.ZRANGE(key, 0, 10, "WITHSCORES", (err, res) => {
                if (err){
                    reject(err);
                } 
                if (!res) {
                    console.log(`Redis > retrieveSet > ${key} >  not found`);
                }
                resolve(res);
            })
        })
    }

    async getOne(key, from, to) {
        from = from || 0;
        to   = to || from + 100;

        return new Promise((resolve, reject) => {
            this.client.ZRANGEBYSCORE(key, from, to, "WITHSCORES", function (err, obj) {
                if (err) {
                    console.error(`Redis > getOne > ${key} > error : ${err}`);
                    reject(err);
                }
                if (!obj) {
                    console.log(`Redis > getOne > ${key} >  not found`);
                }
                resolve(obj);
            });
        });
        
    }
}

module.exports = Redis;
