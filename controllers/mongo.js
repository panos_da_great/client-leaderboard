const { MongoClient, Db } = require('mongodb');
const mongoUrl            = require('../config/configuration.json').mongo;
const events              = require('events');
const eventEmitter        = new events.EventEmitter();

MongoClient.connect(mongoUrl, {
    useNewUrlParser:    true,
    useUnifiedTopology: true,
}, function (err, db) {

    if (err) {
        console.log(err);
        return process.exit(-1);
    }

    console.log(`mongoController > Connection Established @ ${ mongoUrl }`);

    if (db instanceof MongoClient) {
        mongoClient = db;
        _db         = db.db();
    }
    if (db instanceof Db) {
        _db         = db;
        mongoClient = new MongoClient();
    }

    eventEmitter.emit('MongoClientConnected', _db);

    _db.on('close', async function () {
        console.log(`mongoController > Connection Closed @ ${ mongoUrl }`);
        eventEmitter.emit('MongoClientClosed');
    });

});

const getCollectionControler = name => {
    return () => {
        return _db.collection(name);
    };
};

const connect_promise = new Promise(res => {
    eventEmitter.on('MongoClientConnected', () => {
        res();
    });
});

const awaitConnection = () => connect_promise;

const mongoController = {
    eventEmitter,
    awaitConnection,
    //Collections
    Users:                          getCollectionControler('users'),
    db:          () => _db,
    mongoClient: () => mongoClient,

    getCollectionControler,
}

module.exports = mongoController;
