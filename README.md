# CLIENT LEADERBOARD ASSIGNMENT

## WAPPIER

### How to deploy API in 3 steps

- Clone repository
- Install dependencies
- Run ecosystem file with pm2

```
git clone git clone https://panos_da_great@bitbucket.org/panos_da_great/client-leaderboard.git
cd client-leaderboard
npm install
pm2 start ecosystem.config.js
```
