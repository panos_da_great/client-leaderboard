const express           = require('express');
const moment            = require('moment');

const RedisClient   = require("./controllers/redis");
const mongodbClient = require("./controllers/mongo");
const configuration = require('./config/configuration.json');

const Leaderboard   = require('./LeaderBoard');

const app           = express();
const redis         = new RedisClient(configuration.redis);

const API_PORT      = configuration.api_port;

const redisOperations = require("./data-control/redis")(redis);
const dbOperations    = require('./data-control/dbOperations');

const getRedisSetName = () => `lb_${moment.utc().format("YYYYMMDD-HH")}`;

app.use( async(req, res, next) => {
    const key = getRedisSetName();
    const { totalUsers } = await redisOperations.getTotalUsers(key);
    
    res.setHeader("Total-number-of-users-in-the-Leaderboard", totalUsers.toString());
    res.setHeader("Time-remaining-since-current-hour-end", `${60 - new Date().getMinutes()} mins`);
    next();
});

app.get("/api/leaderboard/:page", async (req, res) => {

    const key       = getRedisSetName() ;
    
    let status      = 200;
    let page        = req.params.page
    let response    = { page, data: [] };
    
    try {
        const lbLogic = new Leaderboard(req, res);

        const { redisData } = await redisOperations.retrieveRevPointsList(key, lbLogic.getPaginationIndexes())
        
        lbLogic.setRedisData(redisData);

        const { userData } = await dbOperations.fetchUsers(lbLogic.getObjectIds());
        
        lbLogic.formatUsers(userData);

        response = { ...response, data: lbLogic.getData() };

    } catch(e) {
        console.error(`Expception > ${e}`)
        status = 500;
        response.error = e.message  || "INTERNAL_ERROR";
    } finally {
        res.status(status).json(response)
    }
    
})

mongodbClient.eventEmitter.on("MongoClientConnected", async (db) => {
    app.listen(API_PORT, () => { console.log(`client-leaderboard API listens at port ${API_PORT}`) });
});