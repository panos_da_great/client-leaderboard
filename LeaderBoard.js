const moment            = require("moment");
const { ObjectId }      = require('mongodb');
const paginationLimit = require('./config/configuration.json').pagination_limit;;

class Leaderboard {
    constructor(req, res) {
        this.key        = `lb_${moment.utc().format("YYYYMMDD-HH")}`;
        this.page       = req.params.page;
        this.lastPage   = Math.ceil(res.getHeader("Total-number-of-users-in-the-Leaderboard") / paginationLimit);
        this.rank       = (this.page - 1) * paginationLimit 
        this.validateCall();

        this.data = [];
    }

    getData() {
        return this.data;
    }

    getObjectIds(){
        return this.objectsIds || [];
    }

    getKey(){
        return this.key;
    }

    getPaginationIndexes(){
        return this.paginatonIndexes;
    }

    validateCall() {
        // Validate Page Requested
        if ( isNaN(this.page) || Number(this.page) % 1 !== 0) { //throwing error if page is not numerical
            throw new Error("INVALID_PAGE");
        } else {
    
            this.page = Number(this.page);
            //page does not exist
            if (this.page > this.lastPage || this.page <= 0) {
                throw new Error("PAGE_DOES_NOT_EXIST");
            }
        }
        this.setPaginationIndexes();
    }

    setPaginationIndexes() {
        this.paginatonIndexes = {
            start: (this.page - 1) * paginationLimit,
            stop:  (this.page * paginationLimit) - 1
        }
    }

    async setRedisData(redisData) {
        this.redisData = redisData;
        if (this.redisData.length == 0) {
            throw new Error("PAGE_EMPTY");
        }
        this.objectsIds = redisData.filter((item, i) => i % 2 == 0).map(item=> { return ObjectId(item) })
    }

    formatUsers(userData){
        let Rank = this.paginatonIndexes.start + 1;

        for (let i = 0; i <  this.redisData.length; i++ ) {
            const UserId = this.redisData[i];
            i++;
            const Points = this.redisData[i];

            const user = userData.find(({_id }) => _id.toString() == UserId );

            this.data.push({
                UserId,
                Rank,
                "Full name":   user ? user.name   : "Unknown",
                "Avatar URL":  user ? user.avatar : null,
                Points
            });

            Rank++;
        }
    }
    
}

module.exports = Leaderboard;