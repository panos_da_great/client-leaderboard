const successRes = data => {
    return {
        success_code: 'OK',
        ...data,
    };
};

const errorRes = (code, e )  => {
    return { success_code: code, error: e };
};

module.exports = { successRes, errorRes }