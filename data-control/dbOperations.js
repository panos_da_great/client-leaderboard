const mongo     = require("../controllers/mongo");

const { successRes, errorRes } = require("./dataResponses");

module.exports = {
    fetchUsers: async(objectIdsArr) => {
        try {
            const userData = await mongo.Users().find({  _id: { $in: objectIdsArr } }).limit(25).toArray();
            return successRes({ userData });
        } catch(e) {
            throw errorRes("MONGO_FAILED", e);
        }
    }
} 

