const { successRes, errorRes } = require("./dataResponses");

module.exports = (redis) => ({
    getTotalUsers: async(key) => {
        try{ 
            const totalUsers = await redis.getListLength(key, "-", "+");
            console.log("TotalUsers: ", totalUsers);
            return successRes({ totalUsers });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    },

    retrieveRevPointsList: async(key, { start, stop }) => {
        try {
            const redisData = await redis.retrieveRevPointsList(key, start, stop);
            return successRes({ redisData });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    },

    retrieveRevList: async(key, { start, stop }) => {
        try {
            const redisData = await redis.retrieveRevList(key, start, stop);
            return successRes({ redisData });
        } catch(e) {
            console.error(`Redis Data Control > retrieveRevPointsList : ${e}`);
            return  errorRes("REDIS_FAILED", e);
        }
    }


})